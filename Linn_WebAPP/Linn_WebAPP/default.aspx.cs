﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Linn_WebAPP
{
    public partial class _default : System.Web.UI.Page
    {
        static Guid ApplicationId = new Guid("131012f7-98a6-47e0-bf4b");
        static Guid ApplicationSecret = new Guid("e20de919-74e5-4a1d-a9ea");
        Models.MacroLinnworks macroLinnworks = new Models.MacroLinnworks();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["TOKEN"] == null && Session["mysession"] != null)
            {
                SetError("token is not provided");
                return;
            }
            if (Session["mysession"] == null)
            {
                var baseSession = LinnworksAPI.AuthMethods.AuthorizeByApplication(ApplicationId, ApplicationSecret, new Guid(Request.QueryString["TOKEN"]));
                Models.MySession storeSession = new Models.MySession()
                {
                    AuthToken = baseSession.Token,
                    Email = baseSession.Email,
                    LinnworksId = baseSession.Id,
                    Server = baseSession.Server
                };
                Session["mysession"] = storeSession;
                macroLinnworks.Execute(baseSession.Token, baseSession.Server);
            }
           
          
            txtEmail.Text = ((Models.MySession)Session["mysession"]).Email;
            txtLinnworksId.Text = ((Models.MySession)Session["mysession"]).LinnworksId.ToString();
            
        }
        void SetError(string error)
            {
                errorPanel.Visible = true;
                lblError.Text = error;
            }
        
        }
    }
