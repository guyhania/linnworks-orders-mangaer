﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Linn_WebAPP._default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel ID="errorPanel" runat="server" Visible="false">
            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
        </asp:Panel> 
        <div>
            <h1>My App</h1>
                Email: <asp:Label ID="txtEmail" runat="server" Text=""></asp:Label><br />
                Linnworks Id: <asp:Label ID="txtLinnworksId" runat="server" Text=""></asp:Label><br />
        </div>
    </form>
</body>
</html>
