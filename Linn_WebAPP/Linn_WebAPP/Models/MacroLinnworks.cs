﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LinnworksAPI;


namespace Linn_WebAPP.Models
{
    public class MacroLinnworks 
    {
        OrderDetails openOrders;
        FieldsFilter filter = new FieldsFilter();
        FieldSorting sort = new FieldSorting();
        List<FieldSorting> sortList = new List<FieldSorting>();
        Guid center =new Guid("00000000-0000-0000-0000-000000000000");
        List<Guid> ordersGuid = new List<Guid>();


        public void Execute(Guid ApiToken,String server)
        {
            var Warehouse = LinnworksAPI.ReturnsRefundsMethods.GetWarehouseLocations(ApiToken, server);
            foreach (var o in Warehouse)
            {
                System.Diagnostics.Debug.WriteLine(o);
                
                ordersGuid =  OrdersMethods.GetAllOpenOrders(filter,sortList, center, null,ApiToken, server);
                foreach (var orderId in ordersGuid)
                {
                    var order = OrdersMethods.GetOrderById(orderId, ApiToken, server);
                    System.Diagnostics.Debug.WriteLine("Customer info: " + order.CustomerInfo.ChannelBuyerName + " Order ID: " + order.NumOrderId);
                }
            }
        }
    }
}